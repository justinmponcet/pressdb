# PressDB

Base de données pour stocker et filtrer des articles de presse.

Installation :

- Sur un serveur LAMP ou équivalent (nécessite PHP).
- Installer les dépendances composer (`composer install`)
- Si pas en localhost les identifiants par défaut est admin/admin. Il faut modifier index.php pour changer.

# Changelog

- 30 mars 2024 : possibilité de rajouter plusieurs catégories
- 3 novembre 2023 : ajout import html europresse
- 17 octobre 2023 : enhance import
- 4 octobre 2023 : Visualisation en onglet + possibilité de merge catégories et journaux + nom journal auto export
- 6 Septembre 2023 : première release publique
